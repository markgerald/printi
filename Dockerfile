FROM php:7.0-fpm

RUN apt-get update && apt-get install -y \
    zlib1g-dev \
    && docker-php-ext-install zip

# install composer
RUN curl -sS https://getcomposer.org/installer | php -- --filename=composer --install-dir=/bin
ENV PATH /root/.composer/vendor/bin:$PATH

EXPOSE 9000