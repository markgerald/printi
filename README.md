# Intro
This application was developed using the Silex PHP micro-framework. The PHP Twig front-end framework was used for views. The MVC Standard was used to organize the standardization of codes. For the frontend, the framework bootstrap was used, to stylize.

#### Run Application

This PHP application can be run with Docker. To do this, install Docker correctly, and run the following commands, inside the application folder:
> docker-compose up -d --build

> docker exec -it php7-printi bash

> composer install

After this commands, the application is ready to run in http://localhost:8095