<?php

namespace App\Controllers;

use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Github\Client;

/**
 * Class IndexController
 * @package App\Controllers
 */
class IndexController implements ControllerProviderInterface
{
    /**
     * @param Application $app
     * @return mixed
     */
    public function connect(Application $app)
    {
        $indexController = $app['controllers_factory'];
        $indexController->get("/", array($this, 'index'))->bind('printi_index');

        return $indexController;
    }

    /**
     * @param Application $app
     * @return mixed
     */
    public function index(Application $app)
    {
        $client = new Client();
        $repositories = $client->api('user')->repositories('symfony');

        return $app['twig']->render('index.html.twig', ['repositories' => $repositories]);
    }
}
