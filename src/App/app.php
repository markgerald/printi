<?php

/** Register Providers */
$app->register(new Silex\Provider\ServiceControllerServiceProvider());
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/views',
));

/** Register Routes */
$app->mount( '/', new \App\Controllers\IndexController());
