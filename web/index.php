<?php
define( 'PATH_ROOT', dirname( __DIR__ ) );
define( 'PATH_VENDOR', PATH_ROOT . '/vendor' );

require_once PATH_VENDOR . '/autoload.php';

$app = new Silex\Application();
include_once PATH_ROOT . '/src/App/app.php';

$app['debug'] = true;

$app->run();